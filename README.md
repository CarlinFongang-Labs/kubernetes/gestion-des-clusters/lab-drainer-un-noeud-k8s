# Lab : Drainage d'un nœud  Kubernetes

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______ 

# 1. Qu'est-ce que le drainage ?
Lors de la maintenance des clusters Kubernetes, il peut être nécessaire de retirer temporairement un nœud Kubernetes du service. L'objectif est que toutes les applications continuent de fonctionner même si un nœud est retiré pour maintenance. Le processus de drainage d'un nœud consiste à arrêter de manière gracieuse les conteneurs en cours d'exécution sur le nœud et à les déplacer potentiellement vers un autre nœud pour éviter toute interruption de service. Cela se fait facilement avec la commande kubectl drain suivie du nom du nœud.

# Exécution / Pratique 

Afin de mettre en oeuvre la notion de Drainage de nœud  Kubernetes, nous allons commencé par deployer un pod et ensuite un deployment dans un cluster kubernetes kubeadm, ceux-ci nous servirons d'éléments de test pour la pratique du drainage d'un nœud dans un cluster Kubernetes. 

[Vous pouvez retrouvez la procédure de mise en place d'un cluster kubeadm via ci lien :](https://gitlab.com/CarlinFongang-Labs/kubernetes/lab1.1-install-kubeadm.git) https://gitlab.com/CarlinFongang-Labs/kubernetes/lab1.1-install-kubeadm.git


## 1. Connexion au Control Plan

```bash
ssh -i id_rsa user@public_ip_address
```

## 2. Deployment d'un pod cobaye

### 1. Création d'un manifest de deploiement pour notre pod coboye

```bash
nano pod.yml
```


```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: nginx
    image: nginx:1.14.2
    ports:
    - containerPort: 80
  restartPolicy: OnFailure
```

### 2. Deployement du pod **"my-pod"**

```bash
kubectl apply -f pod.yml
```

>![Alt text](img/image.png)
*Le pod est bien crée*

Nous pouvons vérifier également sa disponibilité


```bash
kubectl get po
```

>![Alt text](img/image-1.png)
*Le pod "my-pod" est bien disponible*


## 3. Mise en place d'un deployment cobaye

### 1. Création d'un deployment avec 2 replicas


```bash
nano deployment.yml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-deployment
  labels:
    app: my-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: my-deployment
  template:
    metadata:
      labels:
        app: my-deployment
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```

### 2. Exécution du deployment

```bash
kubectl apply -f deployment.yml
```
>![Alt text](img/image-2.png)
*Deployment exécuté*

il est possible de vérifier la disponibilité du deployment ainsi que des replicas lancés


```bash
kubectl get deploy,po
```

>![Alt text](img/image-3.png)
*Deployment "my-deployment" et replicas correspondant en cours d'exécution*

Le deploiement d'un pod, ainsi que d'un deployment est réalisé dans ce lab, car le processus de drenage pour chacun est différent.

### 3. Identification des nodes correspondant à chaque pod exécut

Pour avoir les nodes sur lesquels sont exécutés nos pods précédemment lancé, nous allons passé la commande suivante : 

```bash
kubectl get pods -o wide
```
>![Alt text](img/image-4.png)

Une fois la commande passée, nous pouvons identifier l'emplacement de chaque pod ou replicas

le nœud  **k8s-worker1** héberge le replicas **"my-deployment-6694cfb556-694cq"**
le nœud  **k8s-worker2** héberge le pod **"my-pod"** ainsi que le replicas **"my-deployment-6694cfb556-72kg4"**

Nous allons poursuivre ce lab en drainant le node **"k8s-worker2"** sur lequel se trouve notre pod **"my-pod"** et l'un de nos replicas

## 4. Drainage du node sur lequel le pod est en cours d'exécution

Une fois le node sur le quel notre pod tourne à été bien identifié, dans notre cas, le node **"k8s-worker2"** nous allons passer la commande suivante : 

```bash
kubectl drain k8s-worker2 --ignore-daemonsets --force
```
>![Alt text](img/image-5.png)
*Le node "k8s-worker2" à bien été deconnecté du cluster*

En vérifiant de nouveaux l'ensemble des pods ainsi que les nodes disponible 

```bash
kubectl get pods -o wide && kubectl get nodes
```

>![Alt text](img/image-6.png)

L'on peut constater que le node **"k8s-worker2"** ne fait plus parti du cluster, mais également le pod **"my-pod"** ainsi que le replicas **"my-deployment-6694cfb556-72kg4"**.
En environnement de production, ceci peut causer de véritables problème de continuité des services et même de stabilité des stack applicative. 

Remarque : Un nouveau replicas s'est crée sur le node **"k8s-worker1"** ("my-deployment-6694cfb556-8d6wg") en compensation du replicas précédement disponible sur le node **"k8s-worker2"**.

### Que faire dans le cas où l'on reçois des messages d'erreur lié à "DaemonSet" ou à un Pod autonome lors du drainage d'un node ? 

S'il existe des pods géré par DaemonSet, alors il faudra ignoré DaemonSet


```bash
kubectl drain k8s-worker1 --ignore-daemonsets --force
```

Une fois la commande passé, l'on aura en sortie le même résultat que sur la capture précédente nous indiquant que le pod ou le replicat à bien été **"ejecté"** 

## 5. Observation du node drainé

```bash
kubectl get nodes
```

>![Alt text](img/image-7.png)

On remaque le statuts du node indique que la "planificatio nest désactivé", cela signifie que Kubernetes ne programmera pas / ou ne planifiera pas l'execution de pods sur ce nœud , car des taches de maintenance sont en cours dessus.

## 6. Changement du statut du nœud  drainé

Nous allons changé le statut du node drainé afin de le rendre de nouveau disponible dans le cluster pour des deploiement de noveeaux pods

```bash
kubectl uncordon k8s-worker2
```

>![Alt text](img/image-8.png)
*Commande appliquée*

Vérifions à présent la disponibilité du node **k8s-worker2**

````bash
kubectl get nodes
````

>![Alt text](img/image-9.png)
*Le status du node est passé à Ready*

le nœud  **k8s-worker2** est ainsi prêt à acceuillir de nouvelle charge de travail.

Nous pouvons vérifié de nouveau les les pods en cours `kubectl get pods -o wide`

>![Alt text](img/image-10.png)
*Replicas en cours d'exécution*

Nous pouvons constaté qu'il ne s'est pas effectué un réquilibrage de charge, car tous les pods continue de tourner sur le node **k8s-worker1** malgré la disponibilité du second nœud . 

Il est donc important de tenir compte de ce **facteur** lors du drainage d'un nœud  en environnement de production par exemple.